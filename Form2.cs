﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace q1
{
    public partial class dates : Form
    {
        private Dictionary<DateTime, string> datesDic;
        public dates(string path)
        {
            InitializeComponent();
            string[] data = System.IO.File.ReadAllLines(path);
            datesDic = new Dictionary<DateTime, string>();
            string date = "";
            foreach (string s in data)
            {
                date = s.Split(',')[1];
                datesDic.Add(DateTime.ParseExact(date, "M/d/yyyy", CultureInfo.InvariantCulture), s.Split(',')[0]);
            }
        }

        private void monthCalendar1_MouseCaptureChanged(object sender, EventArgs e)
        {
            DateTime date = Convert.ToDateTime(calendar.SelectionRange.Start.ToShortDateString());
            if (datesDic.ContainsKey(date))
            {
                BDay_lbl.Text = "on the chosen date " + datesDic[date] + " celebrates birthday";
            }
            else BDay_lbl.Text = "no one celebrates birthday on the chosen date";
        }

        private void calendar_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void dates_Load(object sender, EventArgs e)
        {

        }

        
    }
}
